const _ = require('lodash');
const sfdx = require('salesforce-alm');
const bunyan = require('bunyan-sfdx-no-dtrace');

const _optsDefaults = {
  softExit: true
};
const _flagDefaults = {
  quiet: true,
  loglevel: 'error'
};

try {
  sfdx.logger.ringbuffer = new bunyan.RingBuffer({
    limit: 5000
  });
  sfdx.logger.addStream({
    type: 'raw',
    stream: sfdx.logger.ringbuffer
  });
} catch (error) {
  const levels = Object.keys(sfdx.logger.levels).join(', ');
  error.message = `${error.message} - ${messages.getMessage('IncorrectLogLevel', levels)}`;
  throw error;
}

const extractSFDXErrors = () => {
  let err = [];
  const logMsgs = sfdx.logger.getBufferedRecords();
  logMsgs.forEach((currLog) => {
    if (currLog.msg) {
      const currMsg = currLog.msg.match(/{(.*?)}(?=[^}]*$)/gs);
      if (Array.isArray(currMsg) && currMsg.length > 0) {
        let st = currMsg[0].replace(/\\+/gs, '\\');
        st = st.replace(/\\\'/gs, '\'');
        try {
          const parsed = JSON.parse(st);
          if (parsed.status === 1) {
            err.push(parsed);
          }
        } catch (e) {
          console.log('Error occurred while parsing JSON string >>>', st);
          console.log(e);
        }
      }
    }
  });
  return err;
};

const _createCommand = (fn) => (flags, opts) => {
  let rejectOnError = false;
  if (_.has(flags, 'rejectOnError')) {
    rejectOnError = flags['rejectOnError'];
    if (typeof rejectOnError === 'string') {
      rejectOnError = rejectOnError.toLowerCase() === 'true';
    }
    delete flags['rejectOnError'];
  }
  const ctx = _.defaults(opts || {}, _optsDefaults);
  ctx.flags = _.defaults(flags || {}, _flagDefaults);
  // Empty logger records before each SFDX call so that the logs from one command execution do not impact the subsequent commands
  sfdx.logger.ringbuffer.records = [];
  return fn(ctx)
    .then((sfdxResult) => {
      if (rejectOnError) {
        const sfdxErrors = extractSFDXErrors();
        if (sfdxErrors.length) {
          throw sfdxErrors;
        }
      }
      // Revert exitCode set by SFDX as we don't want CI scripts to exit due to this
      if (process.exitCode) {
        process.exitCode = 0;
      }
      return sfdxResult;
    })
    .catch((sfdxErr) => {
      // Revert exitCode set by SFDX as we don't want CI scripts to exit due to this
      if (process.exitCode) {
        process.exitCode = 0;
      }
      // Ensure that the error is thrown as an array of plain JS objects, in format => { message: "some error", stack: "stack trace for the error" }
      const errors = require('./process-errors');
      throw errors.processAllErrors(sfdxErr);
    });
};

const sfdxApi = {};
const commandsByTopic = _.groupBy(sfdx.commands, 'topic');
_.forEach(commandsByTopic, (commands, topic) => {
  const sfdxTopic = sfdxApi[topic] = {};
  _.forEach(commands, (command) => {
    sfdxTopic[_.camelCase(command.command)] = _createCommand(command.run);
  });
});

sfdxApi.logger = sfdx.logger;
module.exports = sfdxApi;
