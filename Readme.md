# SFDX-Node

Wrapper for the Salesforce CLI to use in Node.js

## Installation
`npm install sfdx-node`

## Usage
 The Salesforce CLI is based on the Heroku CLI, and is basically a plugin for the Heroku CLI (namespace: force).
 To use the CLI commands within node we wrap the topics, and camelcase the commands. For example `sfdx force:auth:web:login` becomes `sfdx.auth.webLogin()`.

 > sfdx force:TOPIC:COMMAND:SUB -> sfdx.topic.commandSub();

 Additionally option flags can be passed in as an object to the command using the flag names. So `sfdx force:org:create --setdefaultusername` becomes `sfdx.org.create({ setdefaultusername: true})`

 Commands all return a JS Promise.

```javascript
const sfdx = require('sfdx-node');

//authorize a dev hub
sfdx.auth.webLogin({
  setdefaultdevhubusername: true,
  setalias: 'HubOrg'
})
  .then(() => sfdx.source.push()) //push source
  .then(() => {
    // Display confirmation of source push
    console.log('Source pushed to scratch org');
  });
```

## Using `rejectOnError` for promise rejection (in case of a command failure)

Majority of the Salesforce CLI commands, when executed through node, do not reject the promise when an error occurs. They rather resolve with `undefined`. Promise rejection can be forced by passing in the flag `rejectOnError` as `true`. A few Salesforce CLI commands reject the promise as their out-of-the-box behavior, without using this `rejectOnError` flag.

```javascript
const sfdx = require('sfdx-node');

// Pull the remote changes from the scratch org
sfdx.source.pull({
  rejectOnError: true
})
  .then((pullResult) => {
    // Successfully pulled from scratch org
    console.log('Changes pulled from scratch org:', pullResult);
  })
  .catch((pullError) => {
    // Promise rejected in case of conflicts or some other issue while pulling from scratch org
    console.log('Errors occurred during pull operation:', pullError);
  });
```

Rejecting promises is not the default bahavior of this module, unless the promise is rejected by Salesforce CLI command itself. This `rejectOnError` flag needs to be passed in for every command (executed through this module) which should reject the promise when an error occurs during the execution of the command.

### Promise rejection overlapping for parallel calls

When multiple CLI commands are executed in parallel using this module, while also making use of `rejectOnError`, more than one or even all the commands may end up rejecting the promises. This can happen because the errors are shared among all the parallel executions.

To avoid this, make use of `sfdx-node/parallel` module. It works in the same way as the main module, except for the fact that each command is executed in it's own child process. This ensures that each command execution has it's own context and it doesn't share errors with other commands executing in parallel.

```javascript
const sfdx = require('sfdx-node/parallel');

// Get source status for first scratch org
sfdx.source.status({
  targetusername: 'test-user1@example.com',
  rejectOnError: true
})
  .then((statusResult) => {
    // Source status for first scratch org
    console.log('First org source status:', statusResult);
  })
  .catch((statusError) => {
    // Error occurred during source status check for first scratch org
    console.log('First org error:', statusError);
  });

// Get source status for second scratch org
sfdx.source.status({
  targetusername: 'test-user2@example.com',
  rejectOnError: true
})
  .then((statusResult) => {
    // Source status for second scratch org
    console.log('Second org source status:', statusResult);
  })
  .catch((statusError) => {
    // Error occurred during source status check for second scratch org
    console.log('Second org error:', statusError);
  });
```
