var assert = require('chai').assert;
var sfdx = require('../');
var sfdxParallel = require('../parallel');

describe('sfdx', function () {
  it('should have object \'org\'', function () {
    assert.isObject(sfdx.org);
  })
  describe('org', function () {
    it('should have function \'list\'', function () {
      assert.isFunction(sfdx.org.list);
    });
    it('should have function \'open\'', function () {
      assert.isFunction(sfdx.org.open);
    });
  })
});

describe('sfdx parallel', function () {
  it('should have object \'org\'', function () {
    assert.isObject(sfdxParallel.org);
  })
  describe('org', function () {
    it('should have function \'list\'', function () {
      assert.isFunction(sfdxParallel.org.list);
    });
    it('should have function \'open\'', function () {
      assert.isFunction(sfdxParallel.org.open);
    });
  })
});

describe('sfdx command execution', function () {
  it('should resolve even when trying to display a non-existing org', function (done) {
    sfdx.org.display({
      targetusername: 'wrong@username',
    })
      .then((displayResults) => {
        assert.isUndefined(displayResults);
        done();
      })
      .catch(() => {
        done(new Error('Expected method call to resolve.'))
      });
  });
  it('should reject when trying to display a non-existing org', (done) => {
    sfdx.org.display({
      targetusername: 'wrong@username',
      rejectOnError: true,
    })
      .then(() => {
        done(new Error('Expected method call to reject.'))
      })
      .catch((err) => {
        assert.isArray(err);
        assert.include(err[0].message, 'wrong@username', 'Did not receive expected error from the method call.');
        done();
      });
  });
});

describe('sfdx parallel command execution', function () {
  this.timeout(50000);
  it('Without parallel execution, all parallel calls should fail with the same error, even if only one has an actual error', function (done) {
    const x1 = sfdx.org.display({
      targetusername: 'wrong@username',
      rejectOnError: true,
    })
      .then(() => {
        throw new Error('Expected first method call to reject.');
      })
      .catch((err1) => {
        if (err1 instanceof Error && err1.message === 'Expected first method call to reject.') {
          throw err1;
        }
        assert.isArray(err1);
        return err1[0];
      });

    const x2 = sfdx.config.list({
      rejectOnError: true,
    })
      .then(() => {
        throw new Error('Expected second method call to reject.');
      })
      .catch((err2) => {
        if (err2 instanceof Error && err2.message === 'Expected second method call to reject.') {
          throw err2;
        }
        assert.isArray(err2);
        return err2[0];
      });

    Promise.all([x1, x2])
      .then(([x1Result, x2Result]) => {
        assert.isObject(x1Result);
        assert.isObject(x2Result);
        assert.strictEqual(x1Result.message, x2Result.message, 'Expected same error message from both method calls.')
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  it('With parallel execution, only the calls with an error should fail, others should pass without any impact on them', (done) => {
    const x1 = sfdxParallel.org.display({
      targetusername: 'wrong@username',
      rejectOnError: true,
    })
      .then(() => {
        throw new Error('Expected first method call to reject.');
      })
      .catch((err1) => {
        if (err1 instanceof Error && err1.message === 'Expected first method call to reject.') {
          throw err1;
        }
        assert.isArray(err1);
        return err1[0].message;
      });

    const x2 = sfdxParallel.config.list({
      rejectOnError: true,
    })
      .then((listResult) => {
        return listResult;
      })
      .catch((err2) => {
        throw new Error('Expected second method call to resolve.');
      });

    Promise.all([x1, x2])
      .then(([x1Result, x2Result]) => {
        assert.include(x1Result, 'wrong@username', 'Did not receive expected error from first method call.');
        assert.isArray(x2Result);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });
});
